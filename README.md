minimal-cxx-swig-meson
======================

A minimal example of a C++ library wrapped for Java using SWIG and built using
Meson.


Building
--------

Build, test and install using

    meson setup build
    meson compile -Cbuild
    ln -s ../../libs build/libs/app/
    meson test -Cbuild
    meson install -Cbuild

During the setup step, `JAVA_HOME` should point to a valid Java development
kit.


Bugs
----

A no-op build never happens because of a bug in Meson's `jar()` task. See
[`libs/librunner_jni/meson.build`](libs/librunner_jni/meson.build) for an
explanation.

Another problem is that the generated JAR file expects to find the wrapper
library at the relative path `libs/librunner_jni/runner_jni.jar`. Since this
is not the case in the build directory, a symbolic link must be created before
testing.
