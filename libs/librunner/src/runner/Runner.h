#ifndef RUNNER_RUNNER_H
#define RUNNER_RUNNER_H

namespace runner {

class Task;

class Runner
{
public:
  void run(Task& task);
};

}

#endif // RUNNER_RUNNER_H
