#ifndef RUNNER_TASK_H
#define RUNNER_TASK_H

namespace runner {

class Task
{
public:
  virtual ~Task() = default;
  Task& operator=(Task const&) = delete;

  virtual void run() = 0;
};

}

#endif // RUNNER_TASK_H
