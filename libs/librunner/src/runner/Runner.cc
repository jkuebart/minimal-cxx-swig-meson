#include <runner/Runner.h>

#include <runner/Task.h>

namespace runner {

void
Runner::run(Task& task)
{
  return task.run();
}

}
