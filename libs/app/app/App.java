package app;

import runner.Runner;
import runner.Task;

public class App
{
  private static class MyTask extends Task
  {
    @Override
    public void run()
    {
      System.out.println("MyTask.run()");
    }
  }

  public static void main(String args[])
  {
    System.loadLibrary("runner_jni");
    Runner runner = new Runner();
    runner.run(new MyTask());
  }
}
