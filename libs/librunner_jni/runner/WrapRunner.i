%module(directors="1") WrapRunner

%{
#include <runner/Runner.h>
#include <runner/Task.h>
%}

%include <runner/Runner.h>

// Allow using Task as a base for Java classes.
%feature("director") runner::Task;
%include <runner/Task.h>
